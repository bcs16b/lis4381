# LIS 4381 - Mobile Web Application Development

## Bernard Schramm

### Project 2 Requirements:

*Three Parts:*

1. Add edit functionality to A5
2. Add delete functionality to A5
3. Create RSS Feed

#### README.md file should include the following items:

* Screenshot of Carousel;
* Screenshot of index.php;
* Screenshots of edit_petstore.php;
* Screenshot of Failed and Passed Validation;
* Screenshot of Delete Record Prompt;
* Screenshot of Successfully Deleted Record;

#### Assignment Screenshots:

| *Carousel*: | *index.php*: |
| ------------------------------------------- |------------------------------------------- |
| ![Carousel](img/carousel.png) | ![Index](img/index.png) |

| *edit_petstore.php*: | *Failed Validation*: |
| ------------------------------------------- | ------------------------------------------- |
| ![edit_petstore](img/edit.png) | ![Failed Validation](img/failed.png) |

| *Passed Validation*: | *Delete Record Prompt*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Passed Validation](img/passed.png) | ![Delete Prompt](img/delete.png) |

| *Successfully Deleted Record*: | *RSS Feed*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Deleted Record](img/deleted.png) | ![RSS Feed](img/rss.png) |

#### Assignment Links:

[Localhost](http://localhost/repos/lis4381/ "Localhost")