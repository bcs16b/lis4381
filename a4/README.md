# LIS 4381 - Mobile Web Application Development

## Bernard Schramm

### Assignment 4 Requirements:

*Five Parts:*

1. Create a Bootstrap carousel
2. Create a form with client-side validation
3. Complete Skillset 10
4. Complete Skillset 11
5. Complete Skillset 12

#### README.md file should include the following items:

* Screenshot of Main page;
* Screenshots of failed and passed form validation;
* Screenshot of Java Skillset 10;
* Screenshot of Java Skillset 11;
* Screenshot of Java Skillset 12;
* Screenshot of Java Skillset 12 Bonus;

#### Assignment Screenshots:

| *LIS4381 Portal (Main Page)*: |
| ------------------------------------------- |
| ![Main Page](img/main.png) |

| *Failed Validation*: | *Passed Validation*: |
| ------------------------------------------- |------------------------------------------- |
| ![Failed Validation](img/failed.png) | ![Passed Validation](img/passed.png) |

| *Skillset 10:* | *Skillset 11*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Skillset 10 Running](img/ss10_running.png) | ![Skillset 11 Running](img/ss11_running.png) |

| *Skillset 12*: | *Skillset 12 Bonus:* |
| ------------------------------------------- | ------------------------------------------- |
| ![Skillset 12 Running](img/ss12_running.png) | ![Skillset 12 Bonus Running](img/ss12a_running.png) |

#### Assignment Links:

[Localhost](http://localhost/repos/lis4381/ "Localhost")