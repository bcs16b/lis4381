# LIS 4381 - Mobile Web Application Development

## Bernard Schramm

### Project 1 Requirements:

*Four Parts:*

1. Create My Business Card App
2. Complete Skillset 7
3. Complete Skillset 8
4. Complete Skillset 9

#### README.md file should include the following items:

* Screenshot of My Business Card app screen 1 & 2;
* Screenshot of Java Skillset 7;
* Screenshot of Java Skillset 8;
* Screenshot of Java Skillset 9;

#### Assignment Screenshots:

| *Screen 1*: | *Screen 2*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Screen 1](img/screen1.png) | ![Screen 2](img/screen2.png) |

| *Skillset 7:* | *Skillset 8*: | *Skillset 9*: |
| ------------------------------------------- | ------------------------------------------- | ------------------------------------------- |
| ![Skillset 7 Running](img/ss7_running.png) | ![Skillset 8 Running](img/ss8_running.png) | ![Skillset 9 Running](img/ss9_running.png) |