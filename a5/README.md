# LIS 4381 - Mobile Web Application Development

## Bernard Schramm

### Assignment 5 Requirements:

*Four Parts:*

1. Create a form with server-side validation
2. Complete Skillset 13
3. Complete Skillset 14
4. Complete Skillset 15

#### README.md file should include the following items:

* Screenshot of index.php;
* Screenshots of add_petstore_process.php (incudes error);
* Screenshot of Java Skillset 13;
* Screenshot of Java Skillset 13 Bonus;
* Screenshot of Java Skillset 14;
* Screenshot of Java Skillset 15;

#### Assignment Screenshots:

| *Index.php*: | *add_petstore_process.php (that includes error.php)*: |
| ------------------------------------------- |------------------------------------------- |
| ![Index](img/index.png) | ![Error](img/error.png) |

| *Skillset 13*: | *Skillset 13 Bonus*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Skillset 13 Running](img/ss13_running.png) | ![Skillset 13 Bonus Running](img/ss13a_running.png) |

| *Skillset 14 (index.php)*: | *Skillset 14 (process.php)*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Skillset 14 Running](img/ss14_running_a.png) | ![Skillset 14 Running](img/ss14_running_b.png) |

| *Skillset 14 (index.php)*: | *Skillset 14 (process.php)*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Skillset 14 Running](img/ss14_running_c.png) | ![Skillset 14 Running](img/ss14_running_d.png) |

| *Skillset 15 (index.php)*: | *Skillset 15 (process.php)*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Skillset 15 Running](img/ss15_running_a.png) | ![Skillset 15 Running](img/ss15_running_b.png) |

#### Assignment Links:

[Localhost](http://localhost/repos/lis4381/ "Localhost")