# LIS 4381 - Mobile Web Application Development

## Bernard Schramm

### Assignment 3 Requirements:

*Five Parts:*

1. Create Database
2. Create My Event App
3. Complete Skillset 4
4. Complete Skillset 5
5. Complete Skillset 6

#### README.md file should include the following items:

* Screenshot of My Event app screen 1 & 2;
* Screenshot of Java Skillset 4;
* Screenshot of Java Skillset 5;
* Screenshot of Java Skillset 6;

#### Assignment Screenshots:

| *ERD* |
| ------------------------------------------- |
| ![ERD](img/erd.png) |

| *Promo Video*: | *Screen 1*: | *Screen 2*: |
| ------------------------------------------- | ------------------------------------------- | ------------------------------------------- |
| [![Promo](img/promo2.png)](img/promo.mp4) | ![Screen 1](img/screen1.png) | ![Screen 2](img/screen2.png) |

| *Screen 2 alt 1*: | *Screen 2 alt 2*: | *Screen 2 alt 3*: |
| ------------------------------------------- | ------------------------------------------- | ------------------------------------------- |
| ![alt 1](img/screen3.png) | ![alt 2](img/screen4.png) | ![alt 3](img/screen5.png) |

| *Skillset 4:* | *Skillset 5*: | *Skillset 6*: |
| ------------------------------------------- | ------------------------------------------- | ------------------------------------------- |
| ![Skillset 4 Running](img/ss4_running.png) | ![Skillset 5 Running](img/ss5_running.png) | ![Skillset 6 Running](img/ss6_running.png) |

#### Assignment Links:

[A3 MWB File](docs/a3.mwb)

[A3 SQL File](docs/a3.sql)