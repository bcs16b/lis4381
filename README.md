> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4381 - Mobile Web Application Development

## Bernard Schramm

### LIS 4381 Requirements: 

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create a Bootstrap carousel
	- Create a form with client-side validation
	- Complete skillsets 10, 11, 12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create a form with server-side validation
	- Complete skillsets 13, 14, 15

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create business card app
	- Complete skillsets 7, 8, and 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Add edit/delete functionality to A5
	- Create RSS Feed
