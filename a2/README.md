> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Bernard Schramm

### Assignment 2 Requirements:

*Three Parts:*

1. Create Healthy Recipes Android App
2. Complete Skillset 1
3. Complete Skillset 2

#### README.md file should include the following items:

* Screenshot of Healthy Recipes app screen 1 & 2;
* Screenshot of Java Skillset 1;
* Screenshot of Java Skillset 2;

#### Assignment Screenshots:

| *Screen 1*: | *Screen 2*: |
| ------------------------------------------- | ------------------------------------------- |
| ![Screen 1](img/screen1.png) | ![Screen 2](img/screen2.png) |


| [*Skillset 1:*](files/EvenOrOdd.java) | [*Skillset 2*:](files/LargestNumber.java) | [*Skillset 3*:](files/ArraysAndLoops.java) |
| ------------------------------------------- | ------------------------------------------- | ------------------------------------------- |
| ![Skillset 1 Code](img/ss1_code.png) | ![Skillset 2 Code](img/ss2_code.png) | ![Skillset 3 Code](img/ss3_code.png) |
| ![Skillset 1 Running](img/ss1_running.png) | ![Skillset 2 Running](img/ss2_running.png) | ![Skillset 3 Running](img/ss3_running.png) |