import java.util.Scanner;

class EvenOrOdd
{
    public static void main(String args[])
    {
        
        System.out.println("Developer: Bernard Schramm");
        System.out.println("Program evaluates integers as even or odd.");
        System.out.println("Note: Program does *not* check for non-numeric characters.");

        System.out.println("");

        int num;
        Scanner input = new Scanner(System.in);

        System.out.print("Enter integer: ");
        num = input.nextInt();

        if (num % 2 == 0)
        {
            System.out.print(num + " is an even number.");
        }

        else if (num % 2 != 0)
        {
            System.out.print(num + " is an odd number.");
        }
    }
}