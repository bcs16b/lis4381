import java.util.Scanner;

class LargestNumber
{
    public static void main(String args[])
    {
        System.out.println("Developer: Bernard Schramm");
        System.out.println("Program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values.");
        System.out.println();

        int num1, num2;
        Scanner input = new Scanner(System.in);

        System.out.print("Enter first integer: ");
       
        num1 = input.nextInt();

        System.out.print("Enter second integer: ");
        
        num2 = input.nextInt();

        System.out.println();

        if (num1 > num2)
        {
            System.out.print(num1 + " is larger than " + num2);
        }

        else if (num2 > num1)
        {
            System.out.print(num2 + " is larger than " + num1);
        }

        else
        {
            System.out.print("Integers are equal.");
        }

        System.out.println();
    }
}