> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Bernard Schramm

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation [My PHP Installation](http://localhost:9999 "PHP Localhost");
* Screenshot of running Java Hello;
* Screenshot of running Android Studio - My First App;
* git commands w/short descriptions;
* Bitbucket repo links 

	a) this assignment and 

	b) the completed tutorial above (bitbucketstationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates an empty git repository or reinitialize an existing one
2. git status - displays paths that have differences between the index file and>
3. git add - add file contents to the index using the current content on the wo>
4. git commit - records changes to the repository
5. git push - updates remote refs along with associated objects using local refs
6. git pull - incorporates changes from a remote repository into the current br>
7. git clone - clones a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running [My PHP Installation](http://localhost:9999 "PHP Localhost")*:

![AMPPS Installation Screenshot](img/php.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/Nexus_5_API_30_Screenshot.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bcs16b/bitbucketstationlocations/ "Bitbucket Station Locations")
